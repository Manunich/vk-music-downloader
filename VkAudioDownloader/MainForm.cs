﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using VkAudioDownloader.Helpers;
using VkAudioDownloader.Model;
using VkAudioDownloader.Properties;

namespace VkAudioDownloader
{
    public partial class MainForm : Form
    {
        private Thread[] _backgroundThreads;
        private int _threadCount = 7;
        private string _titleTemplate = "VK Audio Downloader ({0}/{1})";
        private bool _isProcessRun;
        private int _trackCount;

        private readonly TypeAssistant _typeAssistant;

        public MainForm()
        {
            InitializeComponent();
            Show();

            _typeAssistant = new TypeAssistant();
            _typeAssistant.Idled += AssistantUserInfoChanged;
            ResetTrackList();
            textBoxUserId.Text = Settings.Default.VK_ID;
            textBoxRemixsid.Text = Settings.Default.Remixsid;
            textBoxPath.Text = Settings.Default.Path;
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (_isProcessRun || _backgroundThreads!=null && _backgroundThreads.Any(item => item.IsAlive))
            {
                _isProcessRun = false;
                return;
            }

            listBoxLoaded.Items.Clear();
            listBoxFailed.Items.Clear();
            var userId = textBoxUserId.Text;
            var remixsid = textBoxRemixsid.Text;
            var path = textBoxPath.Text;
            string playlistId;

            if (comboBoxPlayList.SelectedIndex == 0)
            {
                playlistId = "-1";
            }
            else
            {
                var tracklist = (TrackList)comboBoxPlayList.SelectedItem;
                playlistId = tracklist.Id;
            }

            if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(remixsid))
            {
                MessageBox.Show(@"Укажите Id пользвователя и remixsid");
                return;
            }
            if (string.IsNullOrEmpty(path))
            {
                MessageBox.Show(@"Укажите путь для сохранения аудиотреков");
                return;
            }
            buttonStart.Text = @"Остановить";
            progressBar.Visible = true;
            progressBar.Value = 0;
            listBoxLoaded.Items.Clear();
            listBoxFailed.Items.Clear();
            _isProcessRun = true;
            Application.DoEvents();

            Settings.Default.VK_ID = userId;
            Settings.Default.Remixsid = remixsid;
            Settings.Default.Path = path;
            Settings.Default.Save();
            DirectoryInfo directory = new DirectoryInfo(path);
            directory.Create();
            VK vk = new VK(userId, remixsid);
            var audioTracks = vk.GetAudioTracksList(playlistId);
            progressBar.Maximum = audioTracks.Length;
            Text = String.Format(_titleTemplate, progressBar.Value, progressBar.Maximum);

            _backgroundThreads = new Thread[_threadCount];
            for (var index = 0; index < _threadCount; index++)
            {
                int processId = index;
                var thread = new Thread(() => LoaderThread(vk, audioTracks, path, processId));
                _backgroundThreads[index] = thread;
            }
            _trackCount = 0;
            foreach (var thread in _backgroundThreads)
            {
                thread.Start();
            }
        }

        void LoaderThread(VK vk, AudioTrack[] audioTracks, string path, int index)
        {
            for (var i = index; i < audioTracks.Length; i+=_threadCount)
            {
                var audioTrack = audioTracks[i];
                string audioFileName = audioTrack.AuidoFileName;
                if (!_isProcessRun) break;
                FileInfo fileInfo = new FileInfo(string.Format("{0}\\{1}", path, audioFileName));
                if (fileInfo.Exists)
                {
                    MyInvoke((MethodInvoker)delegate
                    {
                        progressBar.Value++;
                        _trackCount++;
                        Text = String.Format(_titleTemplate, progressBar.Value, progressBar.Maximum);
                    });
                    continue;
                }
                bool isLoaded = false;
                for (int j = 0; j < 5; j++)
                {
                    try
                    {
                        var audioUrl = vk.GetTrackUrl(audioTrack);
                        using (var client = new WebClient())
                        {
                            client.DownloadFile(audioUrl, fileInfo.FullName);
                        }
                        MyInvoke((MethodInvoker)delegate
                        {
                            listBoxLoaded.Items.Insert(0, audioFileName + " (" + j + ")");
                            _trackCount++;
                        });
                        isLoaded = true;
                        break;
                    }
                    catch (Exception)
                    {
                        Thread.Sleep(500);
                    }
                }

                if (!isLoaded)
                {
                    MyInvoke((MethodInvoker)delegate
                    {
                        listBoxFailed.Items.Insert(0, audioFileName);
                    });
                }

                MyInvoke((MethodInvoker)delegate
                {
                    progressBar.Value++;
                    Text = String.Format(_titleTemplate, progressBar.Value, progressBar.Maximum);
                });
            }

            if (_backgroundThreads.Count(item => item.IsAlive)==1)
            {
                MyInvoke((MethodInvoker)delegate
                {
                    buttonStart.Text = @"Загрузить";
                    progressBar.Visible = false;
                    MessageBox.Show(string.Format("Загружено новых треков {0}\r\nТекущее состояние {1}/{2}", listBoxLoaded.Items.Count, _trackCount, progressBar.Maximum));
                });
            }
        }

        #region Track List

        private void ResetTrackList()
        {
            MyInvoke(new MethodInvoker(() =>
            {
                comboBoxPlayList.Items.Clear();
                comboBoxPlayList.Items.Add("Все треки");
                comboBoxPlayList.SelectedIndex = 0;
            }));
        }

        void AssistantUserInfoChanged(object sender, EventArgs e)
        {
            string userId = null;
            string remixid = null;

            MyInvoke(new MethodInvoker(() =>
            {
                userId = textBoxUserId.Text;
                remixid = textBoxRemixsid.Text;
            }));

            VK vk = new VK(userId, remixid);
            var list = vk.GetTrackLists(userId);
            ResetTrackList();
            MyInvoke((MethodInvoker)delegate
            {
                foreach (var trackList in list)
                {
                    comboBoxPlayList.Items.Add(trackList);
                }
            });
        }

        private void UserInfoChanged(object sender, EventArgs e)
        {
            _typeAssistant.TextChanged();
        }

        #endregion

        #region Form Events

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            _isProcessRun = false;
            if (_backgroundThreads != null)
            {
                e.Cancel = _backgroundThreads.Any(item => item.IsAlive);
                if (e.Cancel)
                {
                    MessageBox.Show(@"Как только докачаются треки, которые уже начали качаться - программу можно будет закрыть");
                }
            }
        }

        private void buttonFolderPicker_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog dialog = new FolderBrowserDialog
            {
                SelectedPath = textBoxPath.Text
            })
            {
                dialog.ShowDialog();
                textBoxPath.Text = dialog.SelectedPath;
            }
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            OnSizeChanged(e);
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            listBoxLoaded.Width = Width / 2;
            label5.Left = listBoxFailed.Left + 15;
        }

        #endregion

        #region Help Methods

        void MyInvoke(Delegate method)
        {
            try
            {
                Invoke(method);
            }
            catch (Exception)
            {
                // ignored
            }
        }

        #endregion
    }
}
