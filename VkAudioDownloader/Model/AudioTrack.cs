﻿using System;
using System.Net;
using VkAudioDownloader.Helpers;

namespace VkAudioDownloader.Model
{
    public class AudioTrack
    {
        public AudioTrack(string[] data)
        {
            AudioId = Int32.Parse(data[0]);
            AccountId = Int32.Parse(data[1]);
            TrackName = data[3];
            TrackArtist = data[4];
        }

        public int AudioId { get; set; }
        public int AccountId { get; set; }
        public string TrackName { get; set; }
        public string TrackArtist { get; set; }

        public override bool Equals(object obj)
        {
            var audioTrack = (AudioTrack) obj;
            return AudioId == audioTrack.AudioId;
        }

        public override int GetHashCode()
        {
            return AudioId;
        }

        public string AuidoFileName
        {
            get { return WebUtility.HtmlDecode(string.Format("{0} - {1}.mp3", TrackArtist.Trim().Cut(), TrackName.Trim().Cut())).ToValidFileName(); }
        }

        public string Id
        {
            get { return string.Format("{0}_{1}", AccountId, AudioId); }
        }
    }
}