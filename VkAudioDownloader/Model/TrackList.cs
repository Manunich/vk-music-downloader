﻿namespace VkAudioDownloader.Model
{
    public class TrackList
    {
        public TrackList(string name, string id)
        {
            Id = id;
            Name = name;
        }

        public string Id { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}