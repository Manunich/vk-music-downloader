﻿using System.IO;
using System.Linq;

namespace VkAudioDownloader.Helpers
{
    public static class FileHelper
    {
        public static string ToValidFileName(this string s, char replaceChar = '_', char[] includeChars = null)
        {
            var invalid = Path.GetInvalidFileNameChars();
            if (includeChars != null) invalid = invalid.Union(includeChars).ToArray();
            return string.Join(string.Empty, s.ToCharArray().Select(o => invalid.Contains(o) ? replaceChar : o));
        }

        public static string Cut(this string s)
        {
            return s.Length > 100 ? s.Substring(0, 100) : s;
        }
    }
}