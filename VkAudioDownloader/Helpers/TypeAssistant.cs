using System;
using System.Threading;

namespace VkAudioDownloader.Helpers
{
    public class TypeAssistant
    {
        public event EventHandler Idled = delegate { };
        public int WaitingMilliSeconds { get; set; }
        readonly Timer _waitingTimer;

        public TypeAssistant(int waitingMilliSeconds = 600)
        {
            WaitingMilliSeconds = waitingMilliSeconds;
            _waitingTimer = new Timer(p =>
            {
                Idled(this, EventArgs.Empty);
            });
        }
        public void TextChanged()
        {
            _waitingTimer.Change(WaitingMilliSeconds, Timeout.Infinite);
        }
    }
}