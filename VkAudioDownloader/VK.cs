using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Newtonsoft.Json;
using VkAudioDownloader.Model;

// ReSharper disable InconsistentNaming

namespace VkAudioDownloader
{
    public class VK
    {
        private const string AudioUrl = "https://vk.com/al_audio.php";
        private readonly string _userId;
        private readonly Encoding Windows1251 = Encoding.GetEncoding("windows-1251");
        private readonly string _cookies;

        public VK(string userId, string remixsid)
        {
            _userId = userId;
            _cookies = string.Format("remixsid={0};", remixsid);
        }

        public string UserId { get { return _userId; } }
        
        public TrackList[] GetTrackLists(string userId)
        {
            List<TrackList> list = new List<TrackList>();

            var data = GetPlayListJson(userId);
            int p = data.IndexOf("audio_pl__info", StringComparison.Ordinal);
            while (p>0)
            {
                int index = data.IndexOf("<a href", p, StringComparison.Ordinal) + 32;
                index = data.IndexOf("_", index, StringComparison.Ordinal)+1;
                string trackListId = data.Substring(index, data.IndexOf("\"", index, StringComparison.Ordinal) - index);
                index = data.IndexOf(">", index, StringComparison.Ordinal) + 1;
                string trackListName = data.Substring(index, data.IndexOf("<", index, StringComparison.Ordinal) - index);
                TrackList trackList = new TrackList(trackListName, trackListId);
                list.Add(trackList);

                p = data.IndexOf("audio_pl__info", p+1, StringComparison.Ordinal);
            }

            return list.ToArray();
        }

        public AudioTrack[] GetAudioTracksList(string playlist)
        {
            List<AudioTrack> list = new List<AudioTrack>();
            var items1 = GetAudioTracksList(playlist, 0);
            var items2 = GetAudioTracksList(playlist, 30);

            list.AddRange(items1.Union(items2));

            return list.ToArray();
        }

        private AudioTrack[] GetAudioTracksList(string playlist, int offset)
        {
            var text = GetFullAuidoListJson(playlist, offset);
            Application.DoEvents();
            int p = text.IndexOf("list\":[", StringComparison.Ordinal);
            var json = text.Substring(p + 6, text.IndexOf(",\"hasMore\"", StringComparison.Ordinal) - p - 6);
            return JsonConvert.DeserializeObject<string[][]>(json).Select(item => new AudioTrack(item)).ToArray();
        }

        private string GetFullAuidoListJson(string playlistId, int offset)
        {
            var postData = string.Format("access_hash=&act=load_section&al=1&claim=0&offset={0}&owner_id={1}&playlist_id={2}&type=playlist", offset, _userId, playlistId);
            return AudioRequest(postData);
        }

        private string GetPlayListJson(string userId)
        {
            var postData = string.Format("act=section&al=1&is_layer=0&owner_id={0}&section=playlists", userId);
            return AudioRequest(postData);
        }

        public string GetTrackUrl(AudioTrack track)
        {
            var postData = string.Format("act=reload_audio&al=1&ids={0}", track.Id);
            var res = AudioRequest(postData);
            int p = res.IndexOf("\"https", StringComparison.Ordinal);
            return Regex.Unescape(res.Substring(p + 1, res.IndexOf(",", p, StringComparison.Ordinal) - p - 1));
        }

        private string AudioRequest(string postData)
        {
            var request = (HttpWebRequest)WebRequest.Create(AudioUrl);
            var data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;
            request.Headers.Add("origin", "https://vk.com");
            request.Headers.Add("useragent", "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
            request.Headers.Add("cookie", _cookies);

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();
            var responseStream = response.GetResponseStream();
            return responseStream != null ? new StreamReader(responseStream, Windows1251).ReadToEnd() : null;
        }
    }
}